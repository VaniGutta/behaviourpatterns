package com.java.behavioral.patterns.CORExample1;
import java.util.Arrays;

public class TwoDimension implements Chain{
    private Chain successor;

    public void setSuccessor(Chain c) {
        this.successor=c;
    }
    public void process(Dimension request) {
        if(request.getCoordinate().length==2){
            System.out.println("Two Dimension Coordinate: "+ Arrays.toString(request.getCoordinate()));
        }
        else
        {
            successor.process(request);
        }
    }
}
