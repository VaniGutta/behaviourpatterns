package com.java.behavioral.patterns.CORExample1;

import java.awt.*;

public interface Chain {

    public abstract void setSuccessor(Chain successor);

    public abstract void process(Dimension request);
}

