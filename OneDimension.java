package com.java.behavioral.patterns.CORExample1;

import java.util.Arrays;

public class OneDimension implements Chain{

    private Chain successor;
    @Override
    public void setSuccessor(Chain c) {
        this.successor=c;
    }

    @Override
    public void process(Dimension request) {
        if(request.getCoordinate().length==1){
            System.out.println("One Dimension Coordinate: "+Arrays.toString(request.getCoordinate()));
        }
        else
        {
            successor.process(request);
        }
    }
}
