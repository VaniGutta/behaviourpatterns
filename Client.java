package com.java.behavioral.patterns.CORExample1;

public class Client {
    public static void main (String []args){
        Chain c1= new OneDimension();
        Chain c2=new TwoDimension();
        Chain c3=new ThreeDimension();

        c1.setSuccessor(c2);
        c2.setSuccessor(c3);
        c1.process(new Dimension(5));
        c1.process(new Dimension(10,12));
        c1.process(new Dimension(10,20,30));
    }
}
